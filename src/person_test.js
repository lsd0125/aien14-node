
// const Person = require('./person');
const Person = require(__dirname + '/person');
const Abcd = require(__dirname + '/person');

const p1 = new Person('bill', 22);
const p2 = new Abcd('peter', 25);

console.log( p1.toJSON() );
console.log( '' + p1 );
console.log( p2.toJSON() );
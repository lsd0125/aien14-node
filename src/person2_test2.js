
// const {Person} = require(__dirname + '/person2'); // 只匯入某個功能
const {Person, f1} = require(__dirname + '/person2');


const p1 = new Person('bill', 22);
const p3 = new Person;


console.log( p1.toJSON() );
console.log( '' + p1 );
console.log( f1(8) );
console.log( p3.toJSON() );

require('dotenv').config();
const moment = require('moment-timezone');
const db = require(__dirname + '/db_connect2');

(async ()=>{
    const sql = `INSERT INTO \`address_book\` 
                (\`name\`, \`email\`, \`mobile\`, \`birthday\`, \`address\`, \`created_at\`)
                 VALUES (?, ?, ?, ?, ?, NOW())`;

    for(let i=0; i<100; i++){
        let name = '陳小華'+i;
        let email = `test${i}@test.com`;
        let mobile = '0935' + (100000 + Math.floor(Math.random()*899999));
        const t1 = new Date('1990-01-01');
        const t2 = new Date('2000-12-31');
        const t3 = new Date(t1.getTime() + Math.floor(Math.random()*(t2-t1)))

        await db.query(sql, [name, email, mobile, moment(t3).format('YYYY-MM-DD'), '高雄市']);

    }

    console.log('ok');

})();
require('dotenv').config();

const express = require('express');
const multer = require('multer');
const upload = multer({dest: 'tmp_uploads/'});
const upload2 = require(__dirname + '/upload-image-module');
const fs = require('fs');
const {v4: uuid_v4} = require('uuid');
const session = require('express-session');
const moment = require('moment-timezone');
const db = require(__dirname + '/db_connect2');
const bcrypt = require('bcryptjs');

const app = express(); // 建立伺服器個體

app.set('view engine', 'ejs'); // 指定樣版引擎

// app.set('views', __dirname + '/../views'); // 指定樣版檔案的資料夾

// 定義路由 routes

// body-parser
app.use(express.urlencoded({extended: false}));
app.use(express.json());

// app.use(express.static('public'));
app.use(express.static(__dirname+'/../public'));

app.use(session({
    saveUninitialized: false,
    resave: false,
    secret: 'ksdfhkjdfsdigdogipog495734905',
    cookie: {
        maxAge: 1200000
    }
}));
app.use(require('cors')());

// 自訂的全域 middleware
app.use((req, res, next)=>{
    res.locals.title = '小新的網站';

    res.locals.dateToDateString = d => moment(d).format('YYYY-MM-DD');
    res.locals.dateToDateTimeString = function(d) {
        return moment(d).format('YYYY-MM-DD hh:mm:ss');
    };
    res.locals.session = req.session;

    next();
});


app.get('/', (req, res)=>{
    //res.send(`<h2>Hello ~</h2>`);
    res.locals.title = 'Home - ' + res.locals.title;

    res.render('home', {name: 'Shinder'});
});

app.get('/sales-json', (req, res)=>{

    res.locals.title = '業務員列表 - ' + res.locals.title;
    const sales = require(__dirname + '/../data/sales.json');

    res.render('sales-json', {sales});
});

app.get('/try-qs', (req, res)=>{
    res.json(req.query);
});

app.get('/try-post', (req, res)=>{
    res.render('try-post');
});

app.post('/try-post', (req, res)=>{
    res.json(req.body);
});

app.get('/try-post-form', (req, res)=>{
    res.render('try-post-form');
});

app.post('/try-post-form', (req, res)=>{
    res.render('try-post-form', req.body);
});

app.post('/try-upload', upload.single('avatar'), (req, res)=>{
    if(req.file && req.file.originalname){
        fs.rename(
            req.file.path,
            __dirname+'/../public/img/' + req.file.originalname,
            (error)=>{
                if(error){
                    res.json({error});
                } else {
                    res.json({
                        file: req.file,
                        url: '/img/' + req.file.originalname,
                    });
                }
        });
    } else {
        res.json({error:'沒有上傳檔案'});
    }

});

app.post('/try-uploads', upload.array('photo', 3), (req, res)=>{
    res.json({
        files: req.files,
        body: req.body,
    });
});
app.post('/try-upload2', upload2.single('avatar'), (req, res)=>{
    res.json(req.file);
});

app.get('/try-uuid', (req, res)=>{
    res.send(uuid_v4());
    //res.send(uuid.v4());
});

app.get('/pending', (req, res)=>{
});

app.get('/my-params1/:action?/:id?', (req, res)=>{
    res.json(req.params);
});

app.use(require(__dirname + '/routes/admin2') );
app.use('/aaa', require(__dirname + '/routes/admin2') );
app.use('/address-book', require(__dirname + '/routes/address-book') );
app.use( require(__dirname + '/routes/products') );

app.get(/\/m\/09\d{2}-?\d{3}-?\d{3}$/i, (req, res)=>{
    let u = req.url.slice(3); // 去掉 /m/
    u = u.split('?')[0];  // 去掉 query string
    u = u.split('-').join(''); // 去掉 dash
    res.send(u);
});

app.get('/try-session', (req, res)=>{
    req.session.my_var = req.session.my_var || 0;
    req.session.my_var++;

    res.json({
        my_var: req.session.my_var,
        session: req.session
    });
});

app.get('/try-moment', (req, res)=>{
    const now = new Date();
    const t1 = req.session.cookie.expires;
    const fm = 'YYYY-MM-DD hh:mm:ss';

    res.json({
        now: moment(now).format(fm),
        t1: moment(t1).format(fm),
        now2: moment().tz('Europe/London').format(fm),
    })
});

app.get('/try-db', (req, res)=>{
    db.query("SELECT * FROM address_book LIMIT 5")
        .then(([results, fields])=>{
            res.json(results);
        });
});

app.get('/login', (req, res)=>{
    res.render('login');
});
app.post('/login', async (req, res)=>{
    const output = {
        success: false,
        error: '',
        postData: req.body,
    };
    if(!req.body.account || !req.body.password){
        output.error = '欄位資料不足';
        return res.json(output);
    }

    const sql = "SELECT * FROM admins WHERE account=?";
    const [rs] = await db.query(sql, [req.body.account]);
    if(!rs.length){
        output.error = '帳號錯誤';
        return res.json(output);
    }

    const result = await bcrypt.compare(req.body.password, rs[0].password_hash);
    if(result){
        req.session.admin = {
            account: rs[0].account,
            nickname: rs[0].nickname,
        };
        output.success = true;
    } else {
        output.error = '密碼錯誤';
    }

    res.json(output);
});
app.get('/logout', (req, res)=>{
    delete req.session.admin;
    res.redirect('/');
});

// 404 處理要放在路由定義的最後面
app.use((req, res)=>{
    res.type('text/html').status(404).send(`
    <link rel="stylesheet" href="/fontawesome/css/all.css">
    <h1>404 找不到頁面</h1>
    <h1>
    <i class="fab fa-expeditedssl"></i>
    </h1>
    `);
});

// console.log('3:', process.env.PORT);
const port = process.env.PORT || 3000;
// console.log('4:', port);
app.listen(port, ()=>{
    console.log('Web Server 啟動: ' + port);
});
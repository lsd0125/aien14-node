const bcrypt = require('bcryptjs');

(async ()=>{
    const t1 = new Date;
    const salt1 = await bcrypt.genSalt(8);
    const hash1 = await bcrypt.hash('345678', salt1);
    const t2 = new Date;
    console.log(t2-t1);
    console.log({salt1, hash1});

    const salt2 = await bcrypt.genSalt(12);
    const hash2 = await bcrypt.hash('345678', salt2);
    const t3 = new Date;
    console.log(t3-t2);
    console.log({salt2, hash2});
})();

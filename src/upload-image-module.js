const multer = require('multer');
const {v4: uuidv4} = require('uuid');

// 篩選, 決定副檔名
const extMap = {
    'image/jpeg': '.jpg',
    'image/png': '.png',
    'image/gif': '.gif',
};

const storage = multer.diskStorage({
    destination: (req, file, cb)=>{
        cb(null, __dirname+'/../public/img');
    },
    filename: (req, file, cb)=>{
        const ext = extMap[file.mimetype];
        cb(null, uuidv4()+ext);
    }
});

const fileFilter = (req, file, cb)=>{
    cb(null, !!extMap[file.mimetype]);
}

module.exports = multer({storage, fileFilter});
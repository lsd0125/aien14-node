const express = require('express');
const router = express.Router();

// router middleware
router.use((req, res, next)=>{
    // middleware 要做的事情
    next();
});

router.get('/admin2/:p1?/:p2?', (req, res)=>{
    res.json({
        params: req.params,
        url: req.url,
        baseUrl: req.baseUrl,
    });
});

module.exports = router;




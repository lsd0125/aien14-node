const express = require('express');
const moment = require('moment-timezone');
const db = require(__dirname + '/../db_connect2');

const router = express.Router();

router.get('/products', async (req, res)=>{
    const sql = "SELECT * FROM products";
    const [rs] = await db.query(sql);

    res.render('products', {rs});
});

router.post('/cart/add', async (req, res)=>{
    if(! req.session.cart) {
        req.session.cart = [];
    }

    const output = {
        success: false,
        error: '',
        code: 0, // 追踪程式走到哪
        cart: req.session.cart,
        postData: req.body,
    };

    const pk = req.body.pk; // primary key
    const qty = + req.body.qty;
    if(!qty || qty<1){
        output.error = '數量不能小於 1';
        output.code = 401;
        return res.json(output);
    }

    // 檢查是否已經有該項商品在購物車內
    const ar = req.session.cart.filter(el=> el.sid==pk );
    if(ar.length){
        output.success = true;
        output.code = 210;
        ar[0].quantity = qty;
        return res.json(output);
    }

    const sql = "SELECT * FROM products WHERE sid=?";
    const [rs] = await db.query(sql, [pk]);
    if(!rs.length){
        output.error = '沒有該項商品';
        return res.json(output);
    }
    const item = rs[0];
    item.quantity = qty;

    req.session.cart.push(item);
    output.cart = req.session.cart;
    output.success = true;
    output.code = 230;

    res.json(output);
});
router.get('/cart', async (req, res)=> {
    if(! req.session.cart) {
        req.session.cart = [];
    }
    res.render('cart', {cart: req.session.cart});
});
module.exports = router;
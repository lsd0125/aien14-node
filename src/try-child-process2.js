const {spawn} = require('child_process');

const dir = spawn('python', ['--version']);
// const dir = spawn('ls', ['-al']); // git bash

dir.stdout.on('data', data=>{
    console.log(`stdout: ${data}`);
});

dir.stderr.on('data', data=>{
    console.error(`stdout: ${data}`);
});

dir.on('close',  code=>{
    console.log(`closed: ${code}`);
});

var bcrypt = require('bcryptjs');

// hash() 產生編碼
bcrypt.hash('123456', 8, (err, hash)=>console.log(hash) );

// 比對

const hash = '$2a$08$B9LHQh9icRcTr0Do0yMsLeBJRq/9.CMv2fAc5d/qp74eIdUGAUkuy';
bcrypt.compare('123456', hash, function(err, res) {
    // res === true
    console.log(res);
});